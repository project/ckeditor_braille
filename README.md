#### About
[CKEditor5](https://ckeditor.com/ckeditor-5) plugin that provides ability to write [Braille](https://en.wikipedia.org/wiki/Braille) in editor.
Letters combos are mapped to Braille unicode values on text format's
configuration, once plugin is added to a toolbar.

#### Install and configure
- Install module in usual way (preferably via Composer).
- Edit (or create new) Text format that is using CKEditor5.
- Find Braille icon and drag it to a toolbar to enable plugin.
- Map with letters combos to braille is generated at this point,
as a form elements and can be optionally extended.

#### Usage
Toggle on and off Braille inputs writing in CKEditor
by clicking on Braille icon in the toolbar.
Braille keys are ```f d s j k l``` single or multiple
pressed (1-4 of these pressed together).

### Develop
- Navigate to module's directory and run ```npm install```
- Run ```npm run watch``` and edit files in js/src folder.
