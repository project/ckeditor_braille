import { Command } from 'ckeditor5/src/core';

/**
 * CSS class with braille font.
 *
 * @var string
 */
export const brailleClass = 'braille-font';

/**
 * Braille insert/prepare html/markup command.
 */
export default class BrailleOnCommand extends Command {

  /**
   * @inheritDoc
   */
  execute(firstElement) {
    const editor = this.editor;
    const model = editor.model;
    model.change((writer) => {
      const element = writer.createElement('div', {'class': brailleClass});
      if (!writer.model.hasContent(element)) {
        const root = writer.model.document.getRoot();
        if (firstElement) {
          const isBrailleWrapper = firstElement.is('element', 'div') && firstElement.getAttribute('class') === brailleClass;
          if (!isBrailleWrapper) {
            writer.insert(element, firstElement, 'after');
            const selection = writer.createSelection(element, 'in');
            writer.setSelection(selection.getFirstPosition());

            if (firstElement.is('element', 'paragraph') && firstElement.childCount < 1 && firstElement.index === element.index - 1) {
              writer.remove(firstElement);
            }
          }
        }
        else {
          writer.insert(element, root, 'end');
        }
      }
    });
  }
}
