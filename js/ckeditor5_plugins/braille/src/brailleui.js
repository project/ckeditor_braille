import { Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';
import { keyCodes } from '@ckeditor/ckeditor5-utils/src/keyboard';
import icon from '../../../../icons/braille.svg';
import { brailleClass } from './brailleoncommand';

/**
 * Retrieve escape keys from plugin config.
 *
 * @param {typeof import('@ckeditor/ckeditor5-engine/src/editor/Editor')} editor
 *   CKEditor instance.
 *
 * @return Array
 *   An array with key code names.
 */
export function escapeKeys(editor) {
  return editor.config.get('ckeditor_braille_braille_escape_keys');
}

/**
 * The Braille UI plugin.
 */
export default class BrailleUI extends Plugin {

  /**
   * @inheritDoc
   */
  constructor(editor) {
    super(editor);
    this.set('isBrailleEditing', false);
    this.set('escapeKeys', escapeKeys(editor));
  }

  /**
   * @inheritDoc
   */
  static get pluginName() {
    return 'BrailleUI';
  }

  /**
   * @inheritDoc
   */
  init() {

    const self = this;
    const editor = this.editor;

    this.on('change:isBrailleEditing', (evt, name, isBrailleEditing) => {

      const root = editor.model.document.getRoot();

      // On button state.
      if (isBrailleEditing) {
        const nodes = Array.from(root.getChildren());
        const lastElement = nodes.length ? nodes[nodes.length - 1] : null;
        editor.execute('brailleOn', lastElement);
        editor.editing.view.focus();

        // Invoke key listeners.
        this._braille(editor);
      }
      // Off button state.
      else {

        // Invoke key listeners.
        this._braille(editor);

        const brailleStrict = editor.config.get('ckeditor_braille_braille_strict');
        if (brailleStrict) {
          this._brailleStrict(editor);
        }
        editor.editing.view.focus();
      }
    });

    const buttonFactory = () => {

      const button = new ButtonView();
      button.set(
          {
            label: 'Braille',
            icon: icon,
            tooltip: true,
            isToggleable: true,
          }
      );

      button.bind('isOn').to(self, 'isBrailleEditing');
      button.on('execute', () => {
        self.isBrailleEditing = !self.isBrailleEditing;
      });
      return button;
    };
    editor.ui.componentFactory.add('braille', buttonFactory);
  }

  /**
   * Set listeners for Braille inputs (command).
   *
   * @param {typeof import('@ckeditor/ckeditor5-engine/src/editor/Editor')} editor
   *   CKEditor instance.
   */
  _braille(editor) {

    const self = this;
    let keys = [];
    let braille = {};
    let once = 0;

    editor.editing.view.document.on('keydown', (event, data) => {

      const domKey = data.domEvent.code;

      // A special case to prevent simultaneous press of any key and Space.
      // E.g. it can happen with quite fast typing.
      if (domKey === 'Space') {
        new Promise((resolve) => {
          resolve(event.source.selection);
        }).then((value) => {
          once = 0;
          keys = [];
          // const letSpace = value.anchor.isAtEnd && value.anchor.parent && value.anchor.parent.data && value.anchor.parent.data.trim();
          const letSpace = value.anchor.parent && value.anchor.parent.data && value.anchor.parent.data.trim();
          if (!letSpace) {
            data.preventDefault();
            event.stop();
          }
        });
      }

      if (!self.escapeKeys.includes(domKey) && self.isBrailleEditing) {
        const map = editor.config.get('ckeditor_braille_braille_inputs');
        const keyCodeNames = Object.fromEntries(
            Object.entries(keyCodes).map(([name, code]) => [code, name.charAt(0).toUpperCase() + name.slice(1)])
        );
        Object.values(keyCodes).map(k => {
          if (k === data.keyCode) {
            const key = String(keyCodeNames[data.keyCode]);
            map.map(m => {
              if (m.keys.toUpperCase().indexOf(key.toUpperCase()) > -1) {
                braille[m.keys] = m.keys === 'space' ? ' ' : m.braille;
              }
              if (m.keys.toUpperCase().indexOf(key.toUpperCase()) === 0) {
                if (keys.indexOf(key) < 0) {
                  keys.push(key.toUpperCase());
                }
              }
            });
          }
        });
        data.preventDefault();
        event.stop();
        once = 0;
      }
    });

    editor.editing.view.document.on('keyup', (event, data) => {
      const domKey = data.domEvent.code;
      if (!self.escapeKeys.includes(domKey) && self.isBrailleEditing) {

        data.preventDefault();
        event.stop();

        if (!once) {
          editor.execute('braille', data, braille, keys);
        }
        once = 1;
        keys = [];
      }
    });
  }

  /**
   * Create a new paragraph for Off button state.
   *
   * Do not mix letters and braille characters.
   *
   * @param {typeof import('@ckeditor/ckeditor5-engine/src/editor/Editor')} editor
   *   CKEditor instance.
   */
  _brailleStrict(editor) {
    const root = editor.model.document.getRoot();
    let existing = [];
    // Check for existing braille wrapper.
    Array.from(root.getChildren()).forEach((node) => {
      if (node.getAttribute('class') === brailleClass) {
        existing.push(node);
      }
    });

    editor.model.change((writer) => {
      const newLine = writer.createElement('paragraph');
      writer.insert(newLine, existing[existing.length - 1], 'after');
      writer.setSelection(writer.createPositionAt(root, 'end'));
    });
  }
}
