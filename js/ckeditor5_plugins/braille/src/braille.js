import { Plugin } from 'ckeditor5/src/core';
import BrailleEditing from './brailleediting';
import BrailleUI from './brailleui';

/**
 * Braille plugin.
*/
export default class Braille extends Plugin {

  /**
   * @inheritDoc
   */
  static get requires() {
    return [
      BrailleEditing,
      BrailleUI
    ];
  }

  /**
   * @inheritDoc
   */
  static get pluginName() {
    return 'Braille';
  }

}
