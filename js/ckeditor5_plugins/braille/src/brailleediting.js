import { Plugin } from 'ckeditor5/src/core';
import BrailleCommand from "./braillecommand";
import BrailleOnCommand from "./brailleoncommand";

/**
 * The editing part of the Braille plugin.
 */
export default class BrailleEditing extends Plugin {

  /**
   * @inheritDoc
   */
  init() {

    const editor = this.editor;

    // Register our commands.
    if (!editor.commands.get('brailleOn')) {
      editor.commands.add('brailleOn', new BrailleOnCommand(editor));
    }

    if (!editor.commands.get('braille')) {
      editor.commands.add('braille', new BrailleCommand(editor));
    }

    const registered = editor.model.schema.getDefinition('div');

    // Register braille div container.
    if (!registered) {
      editor.model.schema.register('div', {
        inheritAllFrom: '$block',
        allowIn: ['root'],
        allowAttributes: ['class'],
        allowChildren: ['span', 'br'],
        isBlock: true,
      });
      editor.conversion.elementToElement({model: 'div', view: 'div'});
      editor.conversion.attributeToAttribute({model: 'class', view: 'class'});
    }

  }
}
