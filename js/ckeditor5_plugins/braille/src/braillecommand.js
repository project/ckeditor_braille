import { Command } from 'ckeditor5/src/core';

/**
 * Braille insert command.
 */
export default class BrailleCommand extends Command {

  /**
   * @inheritDoc
   */
  execute(data, braille = {}, keys = []) {

    const editor = this.editor;
    const brailleKeys = keys.sort().join(' ').toUpperCase();
    let content = {};

    Object.keys(braille).map((code) => {
      const sortedCode = code.split(' ').sort();
      if (sortedCode.join(' ').toUpperCase() === brailleKeys) {
        content[braille[code]] = braille[code];
      }
    });

    if (content) {
      const write = writer => {
        const string = Object.keys(content)[0];
        // Insert braille.
        editor.model.insertContent(writer.createText(string));
      };
      editor.model.change(write);
    }
  }

}
