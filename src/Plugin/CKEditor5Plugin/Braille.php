<?php

declare(strict_types=1);

namespace Drupal\ckeditor_braille\Plugin\CKEditor5Plugin;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Render\Element;
use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableInterface;
use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableTrait;
use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\editor\EditorInterface;

/**
 * CKEditor5 Braille plugin configuration.
 *
 * @phpstan-consistent-constructor
 */
class Braille extends CKEditor5PluginDefault implements CKEditor5PluginConfigurableInterface {

  use CKEditor5PluginConfigurableTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    // @phpstan-ignore-next-line
    return \Drupal::service('config.factory')->get('ckeditor_braille.inputs.default')->getRawData();
  }

  /**
   * {@inheritdoc}
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {

    return [
      'ckeditor_braille_braille_inputs' => $this->configuration['braille_map'],
      'ckeditor_braille_braille_strict' => $this->configuration['braille_strict'],
      'ckeditor_braille_braille_escape_keys' => $this->configuration['braille_escape_keys'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {

    $default_values = $this->configuration;

    $form['map_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Braille Map'),
      '#description' => $this->t('Map braille keys into unicode braille.'),
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    $form['map_container'] += $this->generateElements($form_state, 'braille-map-wrapper', 'items', 'map_container');

    $form['braille_escape_keys'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Escape keys'),
      '#description' => $this->t('A list of keys to escape listening to (do not prevent default) when typing Braille.'),
      '#default_value' => !empty($default_values['braille_escape_keys']) ? implode(PHP_EOL, $default_values['braille_escape_keys']) : NULL,
    ];

    $form['braille_strict'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Force Braille characters on new line.'),
      '#description' => $this->t('When selected, Braille and non-Braille characters cannot be on the same line.'),
      '#default_value' => $default_values['braille_strict'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {

    $this->configuration['braille_strict'] = $form_state->getValue('braille_strict');

    if (is_string($form_state->getValue('braille_escape_keys'))) {
      $braille_escape_keys = preg_split('/\R/', $form_state->getValue('braille_escape_keys'));
      if (!empty($braille_escape_keys)) {
        $this->configuration['braille_escape_keys'] = [];
        foreach ($braille_escape_keys as $braille_escape_key) {
          if ($trimmed = trim($braille_escape_key)) {
            $this->configuration['braille_escape_keys'][] = $trimmed;
          }
        }
      }
    }

    $complete_form_state = $form_state instanceof SubformStateInterface ? $form_state->getCompleteFormState() : $form_state;
    $map = $complete_form_state->getValue('map_container') ?? [];

    if (!empty($map['items'])) {
      $items = array_filter($map['items'], function ($v, $k) {
        return is_numeric($k) && !empty($v['keys']);
      }, ARRAY_FILTER_USE_BOTH);
      $this->configuration['braille_map'] = $items;
    }
    else {
      $this->configuration['braille_map'] = [];
    }
  }

  /**
   * Generate form elements.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   * @param string $id
   *   Unique id for elements parent container.
   * @param string $key
   *   Unique key of a field being manipulated.
   * @param string $container
   *   A parent container.
   *
   * @return array
   *   Processed form element.
   */
  protected function generateElements(FormStateInterface $form_state, string $id, string $key, string $container): array {

    $parent_id = Html::getId($id);
    $params = [
      'id' => $parent_id,
      'key' => $key,
      'label' => $this->t('Braille map'),
      'group_class' => 'braille-sort-weight',
    ];

    $elements = [
      '#parents' => [
        $container,
        $key,
      ],
      '#attributes' => [
        'class' => [
          'draggable',
        ],
      ],
    ];

    $elements['keys'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key(s)'),
      '#title_display' => 'invisible',
      '#default_value' => NULL,
      '#attributes' => [
        'class' => ['braille-keys'],
      ],
    ];

    $elements['braille'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Braille'),
      '#title_display' => 'invisible',
      '#default_value' => NULL,
      '#attributes' => [
        'class' => [
          'braille-font',
          'braille-font-admin',
        ],
      ],
    ];

    $elements['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight'),
      '#title_display' => 'invisible',
      '#default_value' => NULL,
      // Classify the weight element for #tabledrag.
      '#attributes' => [
        'class' => [
          $params['group_class'],
        ],
      ],
    ];
    $complete_form_state = $form_state instanceof SubformStateInterface ? $form_state->getCompleteFormState() : $form_state;
    return $this->multipleItems($complete_form_state, $key, $elements, $params);
  }

  /**
   * Process multiple items form element.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   * @param string $key
   *   Unique key of a field being manipulated.
   * @param array $element_child
   *   Element's data pre-defined.
   * @param array $params
   *   An array with additional params/variables.
   *
   * @return array
   *   Processed form element.
   */
  protected function multipleItems(FormStateInterface $form_state, string $key, array $element_child = [], array $params = []): array {

    // Get default configuration.
    $config = $this->configuration['braille_map'] ?? [];

    // Get current form state's values.
    $values = $form_state->getValues();

    $num_items = $this->multipleState($form_state, $key, $config);
    $default_values = $config;

    // Container for multiple items.
    $id = $params['id'] ?? Html::getId($key);
    $group_class = $params['group_class'] ?? $key . '-sort-weight';
    $parents = [];
    if (isset($element_child['#parents'])) {
      $parents = $element_child['#parents'];
    }
    $element = [
      '#parents' => $parents,
    ];

    $element[$key] = [
      '#type' => 'table',
      // '#caption' => $this->t('Items'),
      '#parents' => $parents,
      '#header' => [
        ['data' => $this->t('Key(s)')],
        ['data' => $this->t('Braille')],
        ['data' => $this->t('Weight')],
      ],
      '#empty' => $this->t('There are no items!'),
      // TableDrag: Each array value is a list of callback arguments for
      // drupal_add_tabledrag(). The #id of the table is automatically
      // prepended; if there is none, an HTML ID is auto-generated.
      '#prefix' => '<div id="' . $id . '">',
      '#suffix' => '</div>',
      // '#tableselect' => TRUE,
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $group_class,
        ],
      ],
    ];

    $default_elements = $element_child;
    $label = 'Default';
    for ($delta = 0; $delta <= $num_items; $delta++) {

      $index = $delta;
      $item = NULL;

      if (!empty($config[$key])) {
        $item = !empty($config[$key][$index]) ? $config[$key][$index] : NULL;
      }
      else {
        if (!empty($values[$key])) {
          $item = !empty($values[$key][$index]) ? $values[$key][$index] : NULL;
        }
      }

      // Child item.
      $label = $params['label'] ?? 'Default';
      if (isset($element_child['braille']) && isset($element_child['keys'])) {
        $label = $element_child['#title'] ?? $label;
        $element[$key][$index]['#attributes'] = $default_elements['#attributes'];
        $element[$key][$index]['#parents'] = $default_elements['#parents'];
        $element[$key][$index]['#parents'][] = (string) $index;
        foreach (Element::children($element_child) as $child_key) {
          $element[$key][$index][$child_key] = $default_elements[$child_key];
          if ($child_key == 'weight') {
            $element[$key][$index][$child_key]['#default_value'] = $index;
          }
          else {
            if (isset($default_values[$index][$child_key])) {
              $element[$key][$index][$child_key]['#default_value'] = $default_values[$index]['keys'] == 'space' ? ' ' : $default_values[$index][$child_key];
            }
          }
        }
      }
      // Fallback to default element type - textfield so far.
      else {
        $element[$key][$index] = [
          '#type' => 'textfield',
          '#title' => $this->t('@label label', ['@label' => $label]),
          '#default_value' => $item,
        ];
      }

      if ($delta == $num_items) {
        $element[$key][$index]['keys']['#description'] = $this->t('A new value is unlikely to be needed for the time being, still leaving such option open.');
        $element[$key][$index]['braille']['#description'] = $this->t('Here you can paste any "new" Braille char.');
      }
    }

    // Render common add/remove ajax buttons.
    $params_data = [
      'id' => $id,
      'label' => $label,
    ];
    $this->multipleOps($element, $key, $params_data, $num_items);

    return $element;
  }

  /**
   * Append add/remove buttons to multiple fields.
   *
   * @param array $element
   *   Referenced form element.
   * @param string $key
   *   Unique key of a field being manipulated.
   * @param array $params
   *   An array with additional params/variables.
   * @param int $num_items
   *   Current number of field instances.
   */
  private function multipleOps(array &$element, string $key, array $params, int $num_items = 1): void {

    $element['actions'] = [
      '#type' => 'actions',
    ];

    $element['actions']['add_item'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Braille item'),
      '#limit_validation_errors' => [],
      '#name' => 'op-' . $key,
      '#submit' => [
        [get_class($this), 'addItemSubmit'],
      ],
      '#weight' => 20,
      '#ajax' => [
        'callback' => [get_class($this), 'ajaxCallback'],
        'wrapper' => $params['id'],
      ],
    ];

    if ($num_items > 1) {

      $element['actions']['remove_item'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove Braille item'),
        '#limit_validation_errors' => [],
        '#name' => 'op-' . $key,
        '#submit' => [
          [get_class($this), 'removeItemSubmit'],
        ],
        '#weight' => 20,
        '#ajax' => [
          'callback' => [get_class($this), 'ajaxCallback'],
          'wrapper' => $params['id'],
        ],
      ];
    }
  }

  /**
   * Add field's id or a complete custom field to config.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   * @param string $key
   *   A form element's key.
   * @param array $default_values
   *   An array with current values for the field (in config).
   *
   * @return int
   *   A current number of field items within the form.
   */
  private function multipleState(FormStateInterface $form_state, string $key, array $default_values = []): int {

    // Gather the number of items in the form already.
    $num_items = $form_state->get('num_' . $key);
    // We have to ensure that there is at least one widget.
    if ($num_items === NULL) {
      if (!empty($default_values) && count($default_values) > 1) {
        $num_items = count($default_values);
      }
      else {
        $num_items = 1;
      }
    }

    $form_state->set('num_' . $key, $num_items);
    return $num_items;
  }

  /**
   * Callback for all ajax actions.
   *
   * Returns parent container element for each group.
   */
  public static function ajaxCallback(array &$form, FormStateInterface $form_state): array {
    $trigger = $form_state->getTriggeringElement();
    $parents = array_slice($trigger['#parents'], 0, -2);
    $element = NestedArray::getValue($form['editor']['settings']['subform']['plugins']['ckeditor_braille_braille_inputs'], $parents);
    return $element;
  }

  /**
   * Add multiple element Submit callback.
   */
  public static function addItemSubmit(array &$form, FormStateInterface $form_state): void {
    $trigger = $form_state->getTriggeringElement();
    $key = str_replace('op-', '', $trigger['#name']);
    $form_state->set('multiple_keys', $key);
    $num_items = $form_state->get('num_' . $key);
    $delta = $num_items + 1;
    $form_state->set('num_' . $key, $delta);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Remove multiple element Submit callback.
   */
  public static function removeItemSubmit(array &$form, FormStateInterface $form_state): void {
    $trigger = $form_state->getTriggeringElement();
    $key = str_replace('op-', '', $trigger['#name']);
    $form_state->set('multiple_keys', $key);
    $num_items = $form_state->get('num_' . $key);
    $delta = $num_items - 1;
    $form_state->set('num_' . $key, $delta);
    $form_state->setRebuild(TRUE);
  }

}
